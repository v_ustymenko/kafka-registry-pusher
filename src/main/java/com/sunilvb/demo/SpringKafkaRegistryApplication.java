package com.sunilvb.demo;

import com.ftr.dgb.transactions.generated.external.kafka.schema.SHORTWINDOW;
import com.ftr.dgb.transactions.generated.internal.schema.InternalTransactionInfo;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringKafkaRegistryApplication {

    final static Logger logger = Logger.getLogger(SpringKafkaRegistryApplication.class);

    private static final String TOPIC = "txonline9SHORTWINDOW";

    @Value("${bootstrap.url}")
    String bootstrap;

    @Value("${registry.url}")
    String registry;

    //  @RequestMapping("/orders")
    //    public String doIt(@RequestParam(value = "name", defaultValue = "Order-avro") String name) {
    //
    //        String ret = name;
    //        try {
    //            ret += "<br>Using Bootstrap : " + bootstrap;
    //            ret += "<br>Using Bootstrap : " + registry;
    //
    //            Properties properties = new Properties();
    //            // Kafka Properties
    //            properties.setProperty("bootstrap.servers", bootstrap);
    //            properties.setProperty("acks", "all");
    //            properties.setProperty("retries", "10");
    //            // Avro properties
    //            properties.setProperty("key.serializer", StringSerializer.class.getName());
    //            properties.setProperty("value.serializer", KafkaAvroSerializer.class.getName());
    //            properties.setProperty("schema.registry.url", registry);
    //
    //            ret += sendMsg(properties, name);
    //        } catch (Exception ex) {
    //            ret += "<br>" + ex.getMessage();
    //        }
    //
    //        return ret;
    //    }
    //
    //    private Order sendMsg(Properties properties, String topic) {
    //        Producer<String, Order> producer = new KafkaProducer<String, Order>(properties);
    //
    //        Order order = Order.newBuilder()
    //                .setOrderId("OId234")
    //                .setCustomerId("CId432")
    //                .setSupplierId("SId543")
    //                .setItems(4)
    //                .setFirstName("Sunil")
    //                .setLastName("V")
    //                .setPrice(178f)
    //                .setWeight(75f)
    //                .build();
    //
    //        ProducerRecord<String, Order> producerRecord = new ProducerRecord<String, Order>(topic, order);
    //
    //
    //        producer.send(producerRecord, new Callback() {
    //            @Override
    //            public void onCompletion(RecordMetadata metadata, Exception exception) {
    //                if (exception == null) {
    //                    logger.info(metadata);
    //                } else {
    //                    logger.error(exception.getMessage());
    //                }
    //            }
    //        });
    //
    //        producer.flush();
    //        producer.close();
    //
    //        return order;
    //    }

    public static void main(String[] args) {
        SpringApplication.run(SpringKafkaRegistryApplication.class, args);
    }

    @RequestMapping("/t")
    public String doTrx() {

        String ret = TOPIC;
        try {
            ret += "<br>Using Bootstrap : " + bootstrap;
            ret += "<br>Using Bootstrap : " + registry;

            Properties properties = new Properties();
            // Kafka Properties
            properties.setProperty("bootstrap.servers", bootstrap);
            properties.setProperty("acks", "all");
            properties.setProperty("retries", "10");

            properties.setProperty("batch.size", "16384");
            properties.setProperty("buffer.memory", "33554432");

            properties.setProperty("linhger.ms", "1");
            // Avro properties
            properties.setProperty("key.serializer", StringSerializer.class.getName());
            properties.setProperty("value.serializer", CustomKafkaAvroSerializer.class.getName());
            properties.setProperty("schema.registry.url", registry);
            properties.setProperty("auto.register.schemas", "true");
            properties.setProperty("use.latest.version", "true");

            ret += sendTrx(properties, TOPIC);
        } catch (Exception ex) {
            ret += "<br>" + ex.getMessage();
        }

        return ret;
    }

    private SHORTWINDOW sendTrx(Properties properties, String topic) throws ExecutionException, InterruptedException {
        Producer<String, SHORTWINDOW> producer = new KafkaProducer<>(properties);

        SHORTWINDOW message = getMsg();
        ProducerRecord<String, SHORTWINDOW> producerRecord = new ProducerRecord<>(topic, 0, "string", message);

        Future<RecordMetadata> result = producer.send(producerRecord, (metadata, exception) -> {
            if (exception == null) {
                logger.info("SEND: " + producerRecord);
                logger.info(metadata);
            } else {
                logger.error(exception.getMessage());
            }
        });

        producer.flush();
        producer.close();
        result.get();
        return message;
    }

    private InternalTransactionInfo getMsgFromBuilder() {
        return InternalTransactionInfo.newBuilder()
                                      .setId(UUID.randomUUID().toString())
                                      .setAccountNo("account-no")
                                      .build();
    }

    private SHORTWINDOW getMsg() {
        return SHORTWINDOW.newBuilder()
                          .setMMAUTHLMT(ByteBuffer.wrap("21881307".getBytes()))
                          .setSDACCTNUM("26255371039011")
                          .setSDKEY("524872######1397")
                          .setSDDATEKEY("20210812 16:20:31.001")
                          .setSDTIEBREAKER("100000U00130")
                          .setDDDATE(Instant.parse("2021-08-12T16:20:31Z"))
                          .setNSRULEHITS(ByteBuffer.wrap("1".getBytes()))
                          .setDDAPDATE(Instant.parse("2021-08-12T16:21:36Z"))
                          .setSMFIID("F101")
                          .setSDPRODIND("8")
                          .setMDTRANAMT1(ByteBuffer.wrap("15.000".getBytes()))
                          .setMDTRANAMT3(ByteBuffer.wrap("220.00".getBytes()))
                          .setSMUSERPRIGOVTID("DEBIT")
                          .setSDORIGCRNCYCDE("980")
                          .setSDB24SRC("980")
                          .setSDAUTHUNAUTHIND("0")
                          .build();
    }
}
