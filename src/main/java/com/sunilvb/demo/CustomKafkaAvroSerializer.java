package com.sunilvb.demo;

import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.NonRecordContainer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Instant;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.LogicalTypes;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.errors.SerializationException;

public class CustomKafkaAvroSerializer extends KafkaAvroSerializer {

    @Override
    protected byte[] serializeImpl(String subject, Object object) throws SerializationException {
        Schema schema = null;
        if (object == null) {
            return null;
        } else {
            String restClientErrorMsg = "";

            try {
                schema = this.getSchema(object);
                int id;
                if (this.autoRegisterSchema) {
                    restClientErrorMsg = "Error registering Avro schema: ";
                    id = this.schemaRegistry.register(subject, schema);
                } else {
                    restClientErrorMsg = "Error retrieving Avro schema: ";
                    id = this.schemaRegistry.getId(subject, schema);
                }

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                out.write(0);
                out.write(ByteBuffer.allocate(4).putInt(id).array());
                if (object instanceof byte[]) {
                    out.write(object);
                } else {
                    BinaryEncoder encoder = EncoderFactory.get().directBinaryEncoder(out, (BinaryEncoder) null);
                    Object value = object instanceof NonRecordContainer ? ((NonRecordContainer) object).getValue() : object;
                    Object writer;
                    if (value instanceof SpecificRecord) {
                        writer = new SpecificDatumWriter(schema, addTimeConverterForInstant());
                    } else {
                        writer = new GenericDatumWriter(schema);
                    }

                    ((DatumWriter) writer).write(value, encoder);
                    encoder.flush();
                }

                byte[] bytes = out.toByteArray();
                out.close();
                return bytes;
            } catch (RuntimeException | IOException var10) {
                throw new SerializationException("Error serializing Avro message", var10);
            } catch (RestClientException var11) {
                throw new SerializationException(restClientErrorMsg + schema, var11);
            }
        }
    }

    private SpecificData addTimeConverterForInstant() {
        SpecificData it = new SpecificData();
        it.addLogicalTypeConversion(new InstantAvroConverter());
        return it;
    }

    //MyAvroConverter
    public static class InstantAvroConverter extends Conversion<Instant> {

        public Class<Instant> getConvertedType() {
            return Instant.class;
        }

        public String getLogicalTypeName() {
            return "timestamp-millis";
        }

        public String adjustAndSetValue(String varName, String valParamName) {
            return varName + " = " + valParamName + ".truncatedTo(java.time.temporal.ChronoUnit.MILLIS);";
        }

        public Instant fromLong(Long millisFromEpoch, Schema schema, LogicalType type) {
            return Instant.ofEpochMilli(millisFromEpoch);
        }

        public Long toLong(Instant timestamp, Schema schema, LogicalType type) {
            return timestamp.toEpochMilli();
        }

        public Schema getRecommendedSchema() {
            return LogicalTypes.timestampMillis().addToSchema(Schema.create(Schema.Type.LONG));
        }
    }
}
